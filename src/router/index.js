import Vue from "vue";
import VueRouter from "vue-router";
import Home from "../views/Home.vue";
import Products from "../views/Products.vue";
import SingleProduct from "../views/SingleProduct.vue";
import CartPage from "../views/CartPage.vue";
import AboutUsPage from "../views/AboutUsPage.vue";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "home",
    component: Home
  },
  {
    path: "/products",
    name: "products",
    component: Products
  },
  {
    path: "/products/:id",
    name: "singleProduct",
    component: SingleProduct
  },
  {
    path: "/cart",
    name: "cartPage",
    component: CartPage
  },
  {
    path: "/aboutus",
    name: "aboutUs",
    component: AboutUsPage
  }
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes
});

export default router;
