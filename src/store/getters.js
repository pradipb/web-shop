export default {
  cartCount: ({ cart }) => {
    return cart.length;
  },
  totalPrice: ({ cart }) => {
    let totalPrice = cart.reduce(
      (total, cartItem) => total + cartItem.quantity * cartItem.price,
      0
    );
    return totalPrice;
  }
};
