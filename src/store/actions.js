import { getAllProducts, updateProduct } from "@/services/products.js";
import { addCartItem, getCartItems } from "@/services/cart.js";

export default {
  getProducts: async ({ commit }) => {
    const products = await getAllProducts();
    commit("GET_DATA", products);
  },

  addReview: async ({ commit }, { id, newProduct }) => {
    const updatedProduct = await updateProduct(id, newProduct);
    commit("ADD_REVIEW", updatedProduct);
  },

  getCart: async ({ commit }) => {
    const items = await getCartItems();
    commit("GET_CART", items);
  },

  addToCart: async ({ commit }, newObject) => {
    const addedProduct = await addCartItem(newObject);
    commit("ADD_CART", addedProduct);
  },
  deleteCart: ({ commit }, id) => {
    //await removeCartItem(id); not working because of fake json-server
    commit("REMOVE_CART", id);
  }
};
