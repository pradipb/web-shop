export default {
  GET_DATA: (state, products) => {
    state.products = products;
  },

  ADD_REVIEW: (state, updatedProduct) => {
    state.products.map(product =>
      product.id !== updatedProduct.id ? product : updatedProduct
    );
  },

  GET_CART: (state, items) => {
    state.cart = items;
  },

  ADD_CART: (state, addedProduct) => {
    state.cart.push(addedProduct);
  },

  REMOVE_CART: (state, id) => {
    const index = state.cart.findIndex(cart => cart.id == id);
    state.cart.splice(index, 1);
  }
};
