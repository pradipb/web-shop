import axios from "axios";

const baseUrl = "https://my-json-server.typicode.com/pbohora/data";

const getCartItems = async () => {
  const response = await axios.get(`${baseUrl}/cart`);
  return response.data;
};

const addCartItem = async newObject => {
  const response = await axios.post(`${baseUrl}/cart`, newObject);
  return response.data;
};

const removeCartItem = async id => {
  await axios.delete(`${baseUrl}/cart/${id}`);
};

export { getCartItems, addCartItem, removeCartItem };
