import axios from "axios";

const baseUrl = "https://my-json-server.typicode.com/pbohora/data";

const getAllProducts = async () => {
  const response = await axios.get(`${baseUrl}/products`);
  return response.data;
};

const updateProduct = async (id, newObject) => {
  const response = await axios.put(`${baseUrl}/products/${id}`, newObject);
  return response.data;
};
const getOneProduct = async id => {
  const response = await axios.get(`${baseUrl}/products/${id}`);
  return response.data;
};

export { getAllProducts, updateProduct, getOneProduct };
