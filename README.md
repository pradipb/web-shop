# web-shop

## Project description
It is a simple e-commerce web application with following functionalities:
1. User can select different products 
2. Write review about the product 
3. Add and remove product in the cart

App uses fake Json server and make http request to get and update view. Link for the app is
https://pradipb.gitlab.io/web-shop/

## Project setup
```
yarn install
```

### Compiles and hot-reloads for development
```
yarn serve
```

### Compiles and minifies for production
```
yarn build
```

### Lints and fixes files
```
yarn lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
